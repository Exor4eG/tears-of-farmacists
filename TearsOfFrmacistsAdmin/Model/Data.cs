﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Model
{
    public class Data
    {
        bool Changed = false;
        public TestData testData;
        public User user;
        SFTP sFTP = new SFTP();
        XDocument doc;

        public Data()
        {
            if (!sFTP.Check())
            {
                throw new Exception("Нет связи с сервером");
            }
        }
        
        /// <summary>
        /// Чтение XML файла c данными по фармацевтам
        /// </summary>
        private void ReadXml()
        {
            doc = XDocument.Load(@"Data\Export.xml");
        }

        /// <summary>
        /// Апдейт XML файла после теста
        /// </summary>
        /// <param результат теста="testResult"></param>
        private void UpdateXML(double testResult)
        {
            foreach (XElement pharmacer in doc.Element("Offers").Elements("Offer"))
            {
                XAttribute code = pharmacer.Attribute("Code");
                if (user.id == code.Value)
                {
                    XAttribute result = pharmacer.Attribute("Result");
                    XAttribute time = pharmacer.Attribute("DateTimeTest");
                    XAttribute attempt = pharmacer.Attribute("Attempts");
                    result.Value = testResult.ToString();
                    time.Value = DateTime.Now.ToString();
                    attempt.Value = (user.attempt + 1).ToString();
                }
            }
            doc.Save(@"Data\Export.xml");
        }

        /// <summary>
        /// Апдейт XML файла (внесение кол-ва вопросов и кол-ва попыток)
        /// </summary>
        /// <param Количество вопросов="qCount"></param>
        /// <param Количество попыток="maxAttempt"></param>
        private void UpdateXML(int qCount, int maxAttempt)
        {
            XElement info = doc.Element("Headers").Element("Header");
            XAttribute Question = info.Attribute("Question");
            XAttribute maxAttempts = info.Attribute("MaxAttempts");
            Question.Value = qCount.ToString();
            maxAttempts.Value = maxAttempt.ToString();
            doc.Save(@"Data\Export.xml");
        }

        /// <summary>
        /// Апдейт XML при изменении версии тестов
        /// </summary>
        private void UpdateXML(int curVersion)
        {
            XElement info = doc.Element("Headers").Element("Header");
            XAttribute version = info.Attribute("Question");
            version.Value = curVersion.ToString();
            doc.Save(@"Data\Export.xml");
        }

        /// <summary>
        /// Чтение dat файла
        /// </summary>
        private void Deserialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream(@"Data\Tests.dat", FileMode.OpenOrCreate))
            {
                testData = (TestData)formatter.Deserialize(fs);
            }
        }

        /// <summary>
        /// Запись в dat файл
        /// </summary>
        private void Serialize()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream(@"Data\Tests.dat", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, testData);
            }
        }

        /// <summary>
        /// Загрузка файла XML с SFTP 
        /// </summary>
        private void DownloadXML()
        {
            sFTP.DownloadXml();
        }

        /// <summary>
        /// Выгрузка XML файла на SFTP
        /// </summary>
        private void UploadXML()
        {
            sFTP.UploadXml();
        }

        /// <summary>
        /// Загрузка dat файла с SFTP
        /// </summary>
        private void DownloadDat()
        {
            sFTP.DownloadDat();
        }

        /// <summary>
        /// Выгрузка dat и xml файлов на SFTP
        /// </summary>
        private void UploadAll()
        {
            if (Changed)
            {
                int curVersion = testData.Setting.Version + 1;
                testData.Setting.Version = curVersion;
                UpdateXML(curVersion);
                //дописать сохранение всего
            }
            

        }

    }
}
