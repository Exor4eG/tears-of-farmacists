﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Result
    {
        public Result(string result, string date)
        {
            Result_ = Convert.ToDouble(result);
            Date_ = Convert.ToDateTime(date);
        }
        public Result(double result)
        {
            Result_ = result;
            Date_ = DateTime.Now;
        }
        public double Result_ { get; }
        public DateTime Date_ { get;}
    }
}
