﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Model
{
    class SFTP
    {
        string host;
        string username;
        int port;
        string keyFilename;
        string remoteDirectory;
        ConnectionInfo con;
        string pathLocalXML = @"Data\Export.xml";
        string pathRemoteXML;
        string pathLocalDat = @"Data\Tests.dat";
        string pathRemoteDat;


        /// <summary>
        /// Проверка соединения с сервером
        /// </summary>
        /// <returns></returns>
        public bool Check()
        {
            bool res;
            using (var client = new SftpClient(con))
            {
                client.Connect();
                res = client.IsConnected;
            }
            return res;
        }

        /// <summary>
        /// Загрузка из XML данных по SFTP
        /// Инициализация класа ConnectionInfo
        /// </summary>
        public SFTP()
        {
            UpdateSettings();
            PrivateKeyFile keyFile = new PrivateKeyFile(keyFilename);
            var keyFiles = new[] { keyFile };
            var methods = new List<AuthenticationMethod>();
            methods.Add(new PrivateKeyAuthenticationMethod(username, keyFiles));
            con = new ConnectionInfo(host, port, username, methods.ToArray());
        }

        /// <summary>
        /// Загрузка из локального файла данных по SFTP
        /// </summary>
        void UpdateSettings()
        {
            try
            {
                XDocument xdoc = XDocument.Load(@"Data\Setting.xml");
                XElement setting = xdoc.Element("Setting").Element("SFTP");
                host = setting.Attribute("Ip").Value;
                port = Convert.ToInt32(setting.Attribute("Port").Value);
                username = setting.Attribute("Username").Value;
                keyFilename = setting.Attribute("Keydir").Value;
                pathRemoteXML = setting.Attribute("pathRemoteXML").Value;
                pathRemoteDat = setting.Attribute("pathRemoteDat").Value;
            }
            catch
            {
                host = "91.210.37.195";
                username = "test";
                port = 60222;
                keyFilename = @"Data\privateSSH";
                pathRemoteXML = @"Export.xml";
                pathRemoteDat = @"Tests.dat";
            }
        }

        /// <summary>
        /// Загрузка XML файла по SFTP
        /// </summary>
        public void DownloadXml()
        { 
            using (var client = new SftpClient(con))
            {
                client.Connect();
                using (Stream fileStream = File.OpenWrite(pathLocalXML))
                {
                    client.DownloadFile(pathRemoteXML, fileStream);
                }
                client.Disconnect();
            }
        }

        /// <summary>
        /// Выгрузка XML файла по SFTP
        /// </summary>
        public void UploadXml()
        {
            using (var client = new SftpClient(con))
            {
                client.Connect();
                using (Stream fileStream = File.OpenRead(pathLocalXML))
                {
                    client.UploadFile(fileStream, pathRemoteXML);
                }
                client.Disconnect();
            }
        }

        /// <summary>
        /// Загрузка Dat файла по SFTP
        /// </summary>
        public void DownloadDat()
        {
            using (var client = new SftpClient(con))
            {
                client.Connect();
                using (Stream fileStream = File.OpenWrite(pathLocalDat))
                {
                    client.DownloadFile(pathRemoteDat, fileStream);
                }
                client.Disconnect();
            }
        }

        /// <summary>
        /// Выгрузка Dat файла по SFTP
        /// </summary>
        public void UploadDat()
        {
            using (var client = new SftpClient(con))
            {
                client.Connect();
                using (Stream fileStream = File.OpenRead(pathLocalDat))
                {
                    client.UploadFile(fileStream, pathRemoteDat);
                }
                client.Disconnect();
            }
        }
    }
}
